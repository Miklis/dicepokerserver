//
// Created by miklis on 25.11.20.
//
#include <stdio.h>
#include <stdlib.h>
#include "player.h"
#include "protocol.h"
#include "room.h"
#include <math.h>
#include <string.h>
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <arpa/inet.h>


/*____________________________________________________________________________
  Převede číslo na jeho řetězcovou reprezentaci o zadaném řádu.
  Například číslo 555 s požadovaným řádem 4 převede na "0555".
  Výsledné číslo se pak vrací přes pointer out_str_num.
  ____________________________________________________________________________
 */
void utils_convert_int_to_n_string(int number, int num_order,char *out_str_num){
    int i, j,real_order, tmp;
    char temp_num[4];

    //inicializace na samé 0
    for(i = 0; i < num_order; i++){
        out_str_num[i] = '0';
    }

    for(i = num_order - 1, j = 0; i >= 0 ; i--, j++){
        tmp = number / pow(10.0, i);
        out_str_num[j] = (char)(tmp + '0');
        number = number - (tmp * pow(10.0, i));
    }
}

/*____________________________________________________________________________
  Převedě řezezcovou reprezentaci id místnosti na její číselnou hodnotu.
  Řetězec je brán z bufferu socketu, kam se ukládají přijaté zprávy.

  Vrací int, kdy int je číselná hodnota příslušné řetezcové reprezentace
  ____________________________________________________________________________
 */
int utils_conver_str_room_num_to_int(playersocket_t *playersocket){
    char room_id[ROOM_ID_LEN + 1];
    int result;

    strncpy(room_id, (*playersocket).buffer + PROTOCOL_HEADER_LEN + (PROTOCOL_MES_TYPE_LEN * 2), ROOM_ID_LEN);

    room_id[ROOM_ID_LEN] = '\n';

    result = atoi(room_id);

    return result;
}


/*____________________________________________________________________________
  Získá ze zprávy hodnotu sázky ve string podobě

  Vrací int, kdy 0 je že zpráva je v pořádku -1 správa nemá odpovídající formát
  ____________________________________________________________________________
 */
int  utils_get_dices_values(playersocket_t *p_socket){
    int i;
    char dice_ch[2], ch;
    dice_ch[1] = '\0';

    for(i = 0; i < 5; i++){
        ch = *(p_socket->buffer + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN + i);
        if(ch < '0' || ch > '6'){
            return -1;
        }

    }

    strncpy(dice_ch, p_socket->buffer + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN, 1);
    p_socket->player->dice1 = dice_ch[0];

    strncpy(dice_ch, p_socket->buffer + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN + 1, 1);
    p_socket->player->dice2 = dice_ch[0];

    strncpy(dice_ch, p_socket->buffer + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN + 2, 1);
    p_socket->player->dice3 = dice_ch[0];

    strncpy(dice_ch, p_socket->buffer + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN + 3, 1);
    p_socket->player->dice4 = dice_ch[0];

    strncpy(dice_ch, p_socket->buffer + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN + 4, 1);
    p_socket->player->dice5 = dice_ch[0];

    return 0;
}


/*____________________________________________________________________________
  Získá ze zprávy hodnotu sázky ve string podobě

  Vrací int, kdy 0 je že zpráva je v pořádku -1 správa nemá odpovídající formát;
    1 hráč odmítl přihodit (rf)
  ____________________________________________________________________________
 */
int utils_get_cash_value(playersocket_t *p_socket, room_t *room){
    char temp[3];
    memset(temp, '\0', 3);


    strncpy(temp,p_socket->buffer + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN , 2);
    if(strncmp(temp, "00", 2) == 0
    || strncmp(temp, "40", 2) == 0
    || strncmp(temp, "80", 2) == 0){
        strncpy(p_socket->player->bet_money, temp, 3);
        room->money_total += atoi(temp);
        return 0;
    }else if (strncmp(temp, "rf", 2) == 0){
        return 1;
    }
    return -1;
}


/*____________________________________________________________________________
  Získá ze zprávy informaci o získaných bodech hráče o tom jestli vyhrál nebo ne

  Vrací int, kdy 0 je že zpráva je v pořádku -1 správa nemá odpovídající formát;
  ____________________________________________________________________________
 */
int utils_get_stat_value(playersocket_t *p_socket){
    int i;
    char score[5];
    memset(score, '\0', 5);

//    if(strncmp(p_socket->buffer + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN, "1", 1) == 0
//    || strncmp(p_socket->buffer + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN, "0", 1) == 0){
//        if(strncmp(p_socket->buffer + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN, "1", 1) == 0){
//            p_socket->player->wins++;
//        }
//    }else{
//        return -1;
//    }

    strncpy(score ,p_socket->buffer + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN, 4);

    for(i = 0; i < 4; i++){
        if(score[i] < '0' || score[i] > '9'){
            return -1;
        }
    }

    p_socket->player->score = atoi(score);
    return 0;
}


bool utils_validate_number(char *str) {
    while (*str) {
        if(!isdigit(*str)){ //if the character is not a number, return false
            return false;
        }
        str++;
    }
    return true;
}

bool utils_is_valid_ip_address(char *ipAddress){
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
    return result != 0;
}