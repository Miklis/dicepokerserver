//
// Created by miklis on 13.11.20.
//

#ifndef DICEPOKERSERVER_LOBBY_H
#define DICEPOKERSERVER_LOBBY_H
#include "return_code.h"
#include "room.h"



#define ONE_ROOM_INFO_LEN        4
#define LOBBY_TYPE_LEN           4
#define LOBBY_ROOM_RES_LEN       3
#define LOBBY_TIME_TO_RESEND_MES 3000

//Lobby message type
#define LOBBY_REQS "reqs"
#define LOBBY_INFO "info"
#define LOBBY_ROOM "room"
#define LOBBY_NICK "nick"
#define LOBBY_exit "exit"

//Lobby room message response
#define LOBBY_ROOM_ACC "acc"
#define LOBBY_ROOM_DEN "den"
#define LOBBY_ROOM_STR "str"



return_code lobby_init(int room_num);
void lobby_clean();
int looby_room_num();
void lobby_get_info(char *out_lobby_info);
int lobby_try_connect(playersocket_t *playersocket, playersocket_t **opponent, int room_id);
return_code lobby_get_room(int room_id, room_t **out_room);
void lobby_get_player_room(playersocket_t *p_socket, room_t **out_room, int *id);
void lobby_remove_player_fr_room(playersocket_t *p_socket);
void lobby_clear_room(room_t *room);
void lobby_get_get_opponent(playersocket_t *p_socket, playersocket_t **out_p_socket);
void lobby_return_player_to_lobby(playersocket_t *p_socket, bool refuse_bet);
void lobby_process_player_refuse_bet(playersocket_t *p_socket, playersocket_t *opponent);
void lobby_check_rooms_state();
void lobby_resend_message(float diff);


#endif //DICEPOKERSERVER_LOBBY_H
