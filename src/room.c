#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include "protocol.h"
#include "server.h"
#include "lobby.h"

/*____________________________________________________________________________
  Pokusí se přidat hráče player do zadané místnosti room.

  Vrací int:
  -1 nepodařilo se přidat hráče, protože je místnost plná.
   1 podařilo se přidat a hráč je v místnosti sám.
   2 podařilo se přidat a v místnosti jsou dva hráči.
   3 stejný hráč je již připojen
  ____________________________________________________________________________
 */
int room_try_add_player_to_room(room_t *room, playersocket_t *player, playersocket_t **opponent){
   if(room->player1 != NULL && room->player2 != NULL){
       return -1;
   }else if(room->player1 == NULL && room->player1 == NULL){
        room->player1 = player;
        room->on_move = player;
        room->first = player;
        player->state = WAITING_FOR_OP;
        room->state = ROOM_WAITING;
       return 1;
   }else{
       if(room->player1 != NULL){
           room->player2 = player;
           *opponent = room->player1;
           room->player2->state = PLAYING;
           room->player1->state = PLAYING;
           room->state = ROOM_PLAYING;
           return 2;
       }else{
           room->player1 = player;
           *opponent = room->player2;
           room->player2->state = PLAYING;
           room->player1->state = PLAYING;
           room->state = ROOM_PLAYING;
           return 2;
       }
   }
}


/*____________________________________________________________________________
  Metoda vyhodnotí do jakého stavu se má přepnout místnost na základě
  předchozího stavu.
  ____________________________________________________________________________
 */
void room_resolve_next_state(room_t *room){
    switch (room->gameState) {
        case GAME_STATE_START_ROUND:
            room->gameState = GAME_STATE_ROLL_ALL;
            break;
        case GAME_STATE_ROLL_ALL:
            room->gameState = GAME_STATE_BETS;
            break;
        case GAME_STATE_BETS:
            room->gameState = GAME_STATE_ROLL;
            break;
        case GAME_STATE_ROLL:
            if(room->player1->player->wins == 2 || room->player2->player->wins == 2){
                room->gameState = GAME_STATE_END_GAME;
            }else{
                room->gameState = GAME_STATE_END_ROUND;
            }
            break;
        case GAME_STATE_END_ROUND:
            if(room->player1->player->score != room->player2->player->score){
                if(room->player1->player->score > room->player2->player->score){
                    room->player1->player->wins++;
                }else{
                    room->player2->player->wins++;
                }

                if(room->player1->player->wins == 2 || room->player2->player->wins == 2){
                    lobby_remove_player_fr_room(room->player1);
                    lobby_remove_player_fr_room(room->player2);
                }

            }
            room->gameState = GAME_STATE_START_ROUND;
            break;

        case GAME_STATE_END_GAME:
            lobby_remove_player_fr_room(room->player1);
            lobby_remove_player_fr_room(room->player2);
            break;
    }

    room->on_move = room->first;
    room->sending_to = NULL;
}


/*____________________________________________________________________________
  Odstraní osamoceného hráče z místnosti. Použít například pokud se protivník
  uprostřed hry odpojí.
  ____________________________________________________________________________
 */
void room_remove_lonely_player(playersocket_t * p_socket){
    send_wl(p_socket, PROTOCOL_GAME_OPPO_DISC_NL, PROTOCOL_GAME_OPPO_LEN);
    p_socket->state = IN_LOBBY;
    lobby_remove_player_fr_room(p_socket);
}


/*____________________________________________________________________________
  Pokud je předaná místnost prázdná tak jí metoda vyčistí do původního stavu,
  aby v ní šla začít nová hra.
  ____________________________________________________________________________
 */
void room_clear_room(room_t *room){
    if(room->player2 == NULL && room->player1 == NULL){
        room->first = NULL;
        room->on_move = NULL;
        room->sending_to = NULL;
        room->state = ROOM_EMPTY;
        room->money_total = 0;
        room->gameState = GAME_STATE_START_ROUND;
        room->reise = false;
        room->reised = 0;
        memset(room->message_buf, '\0' ,MAX_MESSAGE_LEN);
    }
}


/*____________________________________________________________________________
  Prohodí hráče v místnosti který je na řadě a který čeká.
  ____________________________________________________________________________
 */
void room_switch_players(room_t *room){

    if(room->on_move == room->player1){
        room->on_move = room->player2;
    }else{
        room->on_move = room->player1;
    }
}


/*____________________________________________________________________________
  Metoda překopíruje obsah bufferu hráče do bufferu místnosti.
  ____________________________________________________________________________
 */
void room_copy_socket_buffer_to_room_buffer(playersocket_t *p_socket, room_t *room){
    memset(room->message_buf, '\0', MAX_MESSAGE_LEN);
    strncpy(room->message_buf, p_socket->buffer, strlen(p_socket->buffer));
    room->message_buf[strlen(p_socket->buffer)] = '\n';
}



void room_process_player_reise(playersocket_t *p_socket, playersocket_t *p_opponet, room_t *room){
    int p_sock_bet = 0;
    int p_oppo_bet = 0;

    if(p_socket->recive_ack == true){
        p_oppo_bet = atoi(p_opponet->player->bet_money);
        p_sock_bet = atoi(p_socket->player->bet_money);
        //zajistí to že server bude připravený přijmout od opponenta další cash message
        if(p_sock_bet > p_oppo_bet){
            p_socket->recive_ack = false;
            p_opponet->recived_mes = false;
            room->reised = p_sock_bet - p_oppo_bet;
            room->reise = true;
        }
    }
}
