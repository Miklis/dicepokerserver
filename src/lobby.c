#include <stdlib.h>
#include "room.h"
#include "player.h"
#include "lobby.h"
#include "protocol.h"
#include "server.h"
#include <string.h>
#include <stdio.h>

/* Pole místností */
room_t *rooms       = NULL;
/* Počet vytvořených místností */
int number_of_rooms = 0;


/*____________________________________________________________________________
 * Metoda vyčistí místnost a vrátí jí do výchozího stavu
  ____________________________________________________________________________
 */
void lobby_clear_room(room_t *room){
    room->player1     = NULL;
    room->player2     = NULL;
    room->first = NULL;
    room->on_move = NULL;
    room->state       = ROOM_EMPTY;
    room->gameState   = GAME_STATE_START_ROUND;
    room->money_total = 0;
    room->reise = false;
    memset(room->message_buf, '\0' ,MAX_MESSAGE_LEN);
}

/*____________________________________________________________________________
  Funkce vytvoří novou lobby o počtu room_num místností. Pointer na záčátek
  pole místností je předán do globální proměné rooms. To samá pro počet
  místností.

  Vrací return_code:
  OK (0) - vše proběhlo v pořádku
  FATAL_ERROR (2) - došlo k chybě s pamětí
  ____________________________________________________________________________
 */
return_code lobby_init(int room_num){
    int i;
    number_of_rooms = room_num;
    rooms = malloc(sizeof(room_t) * room_num);
    if(rooms == NULL){
        return FATAL_ERROR;
    }

    for(i = 0; i < number_of_rooms; i++){
        lobby_clear_room(&rooms[i]);
    }



    return OK;
}

/*____________________________________________________________________________
  Funkce vrací kolik místností bylo vytvořeno
  ____________________________________________________________________________
 */
int looby_room_num(){
    return number_of_rooms;
}


/*____________________________________________________________________________
 Pokusí připojit klienta playersocket do zvolené místnosti room_id.

 Vrací int:
    -1 nepodařilo se klienta připojit,
    1 podařilo se připojit a je v místnosti sám,
    2 podařilo se připojit a není v místnosti sám.
    3 hráč je již do místnosti připojen
  ____________________________________________________________________________
 */
int lobby_try_connect(playersocket_t *playersocket, playersocket_t **opponent, int room_id){
    int id, res;

    id = room_id - 1; //číslování od 0 v poli

    res = room_try_add_player_to_room(&rooms[id], playersocket, opponent);

    return res;
}


/*____________________________________________________________________________
 Metoda vrací přes pointer out_lobby_info řetězec představující tělo zprávy
 protokolové zprávy pro informaci o stavu lobby.
  ____________________________________________________________________________
 */
void lobby_get_info(char *out_lobby_info){
    int i;
    int lobby_info_len = (ONE_ROOM_INFO_LEN * number_of_rooms) + 1;
    char room_id[3];
    char room_state[2];
    char lobby_info[lobby_info_len];
    char room_info[ONE_ROOM_INFO_LEN];



    for(i = 0; i < number_of_rooms; i++){

        //pokud je id jedno číselné
        if(i < 9){
            sprintf(room_id, "0%d", i + 1);
            strncpy(room_info, room_id ,2);
        }else{//dvoučíselné
            sprintf(room_id, "%d", i + 1);
            strncpy(room_info, room_id ,2);
        }

        if(rooms[i].player1 == NULL && rooms[i].player2 == NULL){
            strncpy(room_info + 2, "0" ,1);
        }else if(rooms[i].player1 != NULL && rooms[i].player2 != NULL){
            strncpy(room_info + 2, "2",1);
        }else{
            strncpy(room_info + 2, "1" ,1);
        }
        sprintf(room_state, "%d", (int)rooms[i].state);
        strncpy(room_info + 3, room_state ,1);

        strncpy(lobby_info + ONE_ROOM_INFO_LEN * i, room_info, ONE_ROOM_INFO_LEN);
    }

    strncpy(out_lobby_info, lobby_info, lobby_info_len);
}

/*____________________________________________________________________________
  Na základě parametru id místnosti room_id, vrací přes pointer na pointer
  odkaz požadované místnosti.

  Vrací return_code:
  OK (0) - vše proběhlo v pořádku
  FATAL_ERROR (2) - došlo k chybě s pamětí
  ____________________________________________________________________________
 */
return_code lobby_get_room(int room_id, room_t **out_room){

    if(room_id < 0 || room_id > number_of_rooms){
        return FATAL_ERROR;
    }else{
        *out_room = &rooms[room_id];
        return OK;
    }
}


/**
 * Odstrání hráče z místnosti (případně z místnosti kdyby došlo k nějaké chybě)
 *
 * @param p_socket
 */
void lobby_remove_player_fr_room(playersocket_t *p_socket){
    int i;

    for(i = 0; i < number_of_rooms; i++){
        if(rooms[i].player1 == p_socket){
            rooms[i].player1 = NULL;
        }

        if(rooms[i].player2 == p_socket){
            rooms[i].player2 = NULL;
        }

        if(rooms[i].first == p_socket){
            rooms[i].first = NULL;
        }

        if(rooms[i].on_move == p_socket){
            rooms[i].on_move = NULL;
        }
    }
    p_socket->state = IN_LOBBY;
    p_socket->recive_ack = false;
    p_socket->recived_mes = false;
    p_socket->player->wins = 0;
}


/*____________________________________________________________________________
  Metoda vrátí hráče do lobby pokud odmítne sázku nebo jeho protivník odmítne
  sázku. Liší se ve zprávách který jsou poslány na klienta
  ____________________________________________________________________________
 */
void lobby_return_player_to_lobby(playersocket_t *p_socket, bool refuse_bet){
    p_socket->state = IN_LOBBY;
    if(refuse_bet == true){
        protocol_send_lobby_info(p_socket);
    }else{
        //opponent_socket vrátit do lobby a poslat zprávu o tom že se protivník odpojil
        send_wl(p_socket, PROTOCOL_GAME_OPPO_REFU_NL, PROTOCOL_GAME_OPPO_LEN);
    }


}


/**
 * Vrátí room na základě toho do které patří hráčský socket
 *
 * @param p_socket
 * @param out_room
 */
void lobby_get_player_room(playersocket_t *p_socket, room_t **out_room, int *id){
    int i;

    for(i = 0; i < number_of_rooms; i++){
        if(rooms[i].player1 == p_socket || rooms[i].player2 == p_socket){
            *out_room = &rooms[i];
            *id = i + 1;
            break;
        }
    }
}


/*____________________________________________________________________________
   Metoda nejde oponenta předaného socketu a vrátí pointer na něj.
  ____________________________________________________________________________
 */
void lobby_get_get_opponent(playersocket_t *p_socket, playersocket_t **out_p_socket){
    int i;
    
    for(i = 0; i < number_of_rooms;i++){
        if(rooms[i].player1 == p_socket){
            *out_p_socket = rooms[i].player2;
            break;
        }else if(rooms[i].player2 == p_socket){
            *out_p_socket = rooms[i].player1;
            break;
        }
    }
}


/*____________________________________________________________________________
   Metoda zkontroluje zda jsou místnosti ve správném stavu a případně
   stav opraví. Při opravě mění i stav hráčů v místnosti. Například
   pokud je místnost ve stavu hrajícím a protivník se odpojí,
   tak hráče co tam zůstal informuje o tom že se protivním odpojil a vrátí
   ho do lobby.
  ____________________________________________________________________________
 */
void lobby_check_rooms_state(){
    int i;

    for(i = 0; i < number_of_rooms; i++){
        if(rooms[i].state == ROOM_PLAYING){
            /* Místnost je ve stavu kdy se hraje ale jeden z hráčů odešel */
            if(rooms[i].player1 == NULL && rooms[i].player2 != NULL){
                room_remove_lonely_player(rooms[i].player2);
                room_clear_room(&rooms[i]);
                printf("LOBBY - clear room[%d]\n", i + 1);
            }else if(rooms[i].player2 == NULL && rooms[i].player1 != NULL){
                room_remove_lonely_player(rooms[i].player1);
                room_clear_room(&rooms[i]);
                printf("LOBBY - clear room[%d]\n", i + 1);
            }else if(rooms[i].player1 == NULL && rooms[i].player2 == NULL){
                room_clear_room(&rooms[i]);
                printf("LOBBY - clear room[%d]\n", i + 1);
            }

            /* V místnosti čekal hráč, ale místnost opustil */
        }else if(rooms[i].state == ROOM_WAITING){
            if(rooms[i].player1 == NULL && rooms[i].player2 == NULL){
                room_clear_room(&rooms[i]);
                printf("LOBBY - clear room[%d]\n", i + 1);
            }
        }
    }
}


/*____________________________________________________________________________
 Metoda zpracuje aktuálně čtený socket a socket protivníka
 a navrtárí je do lobby, protože aktuálně hraný socket odmítl sázku.
 Tudíž končí hra.
  ____________________________________________________________________________
 */
void lobby_process_player_refuse_bet(playersocket_t *p_socket, playersocket_t *opponent){
    lobby_remove_player_fr_room(p_socket);
    lobby_remove_player_fr_room(opponent);
    lobby_return_player_to_lobby(p_socket, true);
    //opponent_socket vrátit do lobby a poslat zprávu o tom že se protivník odpojil
    lobby_return_player_to_lobby(opponent, false);
}


/*____________________________________________________________________________
  Metoda prejde všechny místnosti které jsou ve stavu hraní. A zkontroluje
  všechny hráče kterím se má odesílat zpráva. Pokud od nich nepřišla odpověď
  tak se zpráva odešle znovu s nadějí že odpověď přijde.

  Pokus o poslání proběhne každé 3s.
  ____________________________________________________________________________
 */
void lobby_resend_message(float diff){
    int i;
    room_t *room;
    for(i = 0; i < number_of_rooms; i++){
        room = rooms + i;
        if(room->state == ROOM_PLAYING && room->sending_to != NULL && room->sending_to->recive_ack == false){
            room->diff += diff;
            if(room->diff >= LOBBY_TIME_TO_RESEND_MES){
                send_wl(room->sending_to, room->message_buf, strlen(room->message_buf));
                room->diff = 0.0f;
            }
        }
    }
}


/*____________________________________________________________________________
 Metoda zruší a vyčistí lobby
  ____________________________________________________________________________
 */
void lobby_clean(){
    if(rooms == NULL){
        return;
    }

    free(rooms);
    rooms = NULL;
    number_of_rooms = 0;
}
