//
// Created by miklis on 13.11.20.
//

#ifndef DICEPOKERSERVER_ROOM_H
#define DICEPOKERSERVER_ROOM_H
#include "player.h"

#define ONE_ROOM_INFO_SIZE 4
#define ROOM_ID_LEN        2

#define ROOM_STR_WAITING "WAITING"
#define ROOM_STR_PLAYING "PLAYING"

/* Stavy ve kterých se může nacházet místnost */
typedef enum {
    ROOM_EMPTY   = 0,
    ROOM_WAITING = 1,
    ROOM_PLAYING = 2
}room_state_t;

typedef enum {
    GAME_STATE_START_ROUND = 0,
    GAME_STATE_ROLL_ALL    = 1,
    GAME_STATE_BETS        = 2,
    GAME_STATE_REISE       = 3,
    GAME_STATE_ROLL        = 4,
    GAME_STATE_END_ROUND   = 5,
    GAME_STATE_END_GAME    = 6
}game_state_t;


typedef struct theroom{
    playersocket_t *player1;            /* Reference na první hráče */
    playersocket_t *player2;            /* Reference na druhého hráče*/
    playersocket_t *on_move;            /* Hráč který je na tahu */
    playersocket_t *first;              /* Začínající hráč */
    playersocket_t *sending_to;         /* Hráč kterému se má posílat zpráva ze serveru*/
    room_state_t state;                 /* V jakém stavu je místnost jestli se v ní například hraje nebo je prázdná */
    game_state_t gameState;             /* V jaké fázi se nachází hra v místnosti */
    bool reise;                         /* Zvedala se sázka? */
    int reised;                         /* O kolik se zvýšila puvodní sázka */
    int money_total;                    /* Vsazená částka v místnosti */
    float diff;                         /* Čas do znovu poslání zprávy */
    char message_buf[MAX_MESSAGE_LEN];  /* Buffer pro zprávy, kdyby se měli posílat znova */
}room_t;

int room_try_add_player_to_room(room_t *room, playersocket_t *player, playersocket_t **opponent);
void room_resolve_next_state(room_t *room);
void room_remove_lonely_player(playersocket_t * p_socket);
void room_clear_room(room_t *room);
void room_switch_players(room_t *room);
void room_copy_socket_buffer_to_room_buffer(playersocket_t *p_socket, room_t *room);
void room_process_player_reise(playersocket_t *p_socket, playersocket_t *p_opponet, room_t *room);

#endif //DICEPOKERSERVER_ROOM_H
