//
// Created by miklis on 25.11.20.
//

#ifndef DICEPOKERSERVER_UTILS_H
#define DICEPOKERSERVER_UTILS_H

void utils_convert_int_to_n_string(int number, int num_order,char *out_str_num);
int utils_conver_str_room_num_to_int(playersocket_t *playersocket);
int  utils_get_dices_values(playersocket_t *p_socket);
int utils_get_cash_value(playersocket_t *p_socket, room_t *room);
int utils_get_stat_value(playersocket_t *p_socket);
bool utils_is_valid_ip_address(char *ipAddress);
bool utils_validate_number(char *str);

#endif //DICEPOKERSERVER_UTILS_H
