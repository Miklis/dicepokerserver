#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <arpa/inet.h>    //close
#include <time.h>
#include <sys/select.h>
#include "return_code.h"
#include "protocol.h"
#include "lobby.h"

#define SERVER_LIMIT_CLIENTS 1000
#define MAX_PENDIG_CON       5

/* Maximální počet clientů kteří se můžou připojit k serveru */
int maximum_clients = 0;
int addrlen = 0;

/*____________________________________________________________________________
  Udělá nezbytné úkony pro nastartování serveru.
  To znamená, udělá bind ip k serveru, začne naslouchat na portu a
  vytvoří se master socket.

  Vrací return_code:
  OK (0) - vše proběhlo v pořádku
  SERVER_START_ERR (4) - Nepodařilo se nastartovat server
  ____________________________________________________________________________
 */
return_code start_server(int *server_socket, char *ip, char *port){
    struct sockaddr_in addr;
    int result;
    int option = 1;

    inet_pton(AF_INET, ip, &(addr.sin_addr));
    addr.sin_port        = htons(atoi(port));
    addr.sin_family = AF_INET;
   // addr.sin_addr.s_addr = INADDR_ANY;

    /* Vrátí server socket */
    *server_socket = socket(AF_INET , SOCK_STREAM , 0);
    if( *server_socket == 0)
    {
        printf("SERVER: Cant start server - Socket\n");
        return SERVER_START_ERR;
    }

    /* nastaví socket na komunikaci s více klienty */
    if( setsockopt(*server_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&option, sizeof(option)) < 0 )
    {
        printf("SERVER: Cant start server - setcockopt\n");
        return SERVER_START_ERR;
    } else{
        printf("SERVER: setcockopt - OK\n");
    }



    /* přilepí socket k ip adrese */
    result = bind(*server_socket, (struct sockaddr *) &addr, sizeof(struct sockaddr_in));


    if(result == 0){
        printf("SERVER: Bind - OK\n");
    }else{
        printf("SERVER: Cant start server - Bind\n");
        return SERVER_START_ERR;
    }

    /* socket začne naslouchat na specifickém portu */
    result = listen(*server_socket, MAX_PENDIG_CON);

    if(result == 0){
        printf("SERVER: Listen - OK\n");
    }else{
        printf("SERVER: Cant start server - Listen\n");
    }

    return OK;
}

/*____________________________________________________________________________
  Udělá nezbytné úkony pro nastartování serveru.
  To znamená, udělá bind ip k serveru, začne naslouchat na portu a
  vytvoří se master socket.

  Vrací return_code:
  OK (0) - vše proběhlo v pořádku
  SERVER_RUN_ERR (5) - Došlo k chybě při běhu serveru
  ____________________________________________________________________________
 */
return_code connect_new_client(int *master_socket,
                               playersocket_t *p_sockets,
                               fd_set *readfds){
    int i, new_socket;
    struct sockaddr_in peer_addr;
    player_t *new_player;

    /* Něco se stalo na master socketu to znamená že jde o nové připojení */
    if (FD_ISSET(*master_socket, readfds))
    {

        /* Přidání socketu do pole socketů */
        for (i = 0; i < maximum_clients; i++)
        {
            /* socket v poli na indexu i je volný */
            if(p_sockets[i].sd == 0 )
            {
                if ((new_socket = accept(*master_socket, (struct sockaddr *)&peer_addr, (socklen_t*)&addrlen)) < 0)
                {
                    printf("SERVER: Cant start server - Listen\n");
                    return SERVER_RUN_ERR;
                }



                new_player = malloc(sizeof(player_t));
                p_sockets[i].player         = new_player;
                p_sockets[i].sd             = new_socket;
                p_sockets[i].state          = CONNECTING;
                p_sockets[i].cur_char_index = 0;
                p_sockets[i].recived_bytes  = 0;
                p_sockets[i].send_bytes     = 0;
                /* Informace že se připojil nový uživatel */
                printf("%s:%d [connection] -> used sd %d\n" , inet_ntoa(peer_addr.sin_addr) , ntohs(peer_addr.sin_port), new_socket);
                break;
            }
        }
    }

    return OK;
}

/*____________________________________________________________________________
  Ukončí připojení na socketu.
  ____________________________________________________________________________
 */
void server_close_socket(playersocket_t *p_socket, char *message){
    struct sockaddr_in peer_addr;

    lobby_remove_player_fr_room(p_socket);
    //Somebody disconnected , get his details and print
    getpeername(p_socket->sd  , (struct sockaddr*)&peer_addr , (socklen_t*)&addrlen);
    printf("%s:%d [disconnection] -> %s | recived: %luB, send: %luB\n" , inet_ntoa(peer_addr.sin_addr) , ntohs(peer_addr.sin_port), message, p_socket->recived_bytes, p_socket->send_bytes);

    //Close the socket and mark as 0 in list for reuse
    close( p_socket->sd );
    p_socket->sd = 0;
    p_socket->milis_from_last_seen = 0.0f;
    p_socket->cur_char_index = 0;
    p_socket->state = CONNECTING;
    p_socket->send_ping = 0;
    p_socket->cur_mes_body_len = 0;
    free(p_socket->player);
    p_socket->player = NULL;

}

/*____________________________________________________________________________
  Pokud je na některém ze socketů připraveno něco pro příjem tak provede
  příslušné operace zpracování komunikace
  ____________________________________________________________________________
 */
void process_communication(playersocket_t *p_sockets, fd_set *readfds){
    int i, sd;


    for (i = 0; i < maximum_clients; i++)
    {
        sd = p_sockets[i].sd;

        if (FD_ISSET( sd , readfds))
        {
            //Check if it was for closing , and also read the
            //incoming message
            protocol_read_message(&p_sockets[i]);
       }
    }
}

/*____________________________________________________________________________
  Metoda, která se v budoucnu bude starat o "ping"
  ____________________________________________________________________________
 */
void refresh_clients(playersocket_t *clients, float diff){
    int i;

    for(i = 0; i < maximum_clients; i++){
        if(clients[i].sd > 0 && clients[i].state != CONNECTING){
            clients[i].milis_from_last_seen += diff;

            if(clients[i].send_ping >= PLAYER_TRY_PINGS){
                lobby_remove_player_fr_room(&clients[i]);
                server_close_socket(&clients[i], "Lost connection to client");
            }

            if(clients[i].milis_from_last_seen > TIME_BETWEEN_PINGS){
                protocol_send_ping_mes(&clients[i], true);
                clients[i].send_ping++;
                clients[i].milis_from_last_seen = 0.0f;
            }
        }
    }
}


/*____________________________________________________________________________
  Metoda pro odesílání zpráv na socket o zadané délce. Metoda má jako
  přidanou hodnoto logování do console.
  ____________________________________________________________________________
 */
void send_wl(playersocket_t *p_socket, char *mes, int mes_len){
    struct sockaddr_in peer_addr;
    int sd;

    sd = (*p_socket).sd;

    getpeername(sd , (struct sockaddr*)&peer_addr , (socklen_t*)&addrlen);

    printf("%s:%d [sendTo]  -> %s",inet_ntoa(peer_addr.sin_addr), ntohs(peer_addr.sin_port), mes);

    p_socket->send_bytes += mes_len;
    send((*p_socket).sd, mes, mes_len, 0);
}

/*____________________________________________________________________________
  Metoda slouží pro logování přijaté zprávy od socketu do konzole
  ____________________________________________________________________________
 */
void recieved_fr(playersocket_t *p_socket){
    struct sockaddr_in peer_addr;
    int sd;

    sd = (*p_socket).sd;

    getpeername(sd , (struct sockaddr*)&peer_addr , (socklen_t*)&addrlen);

    printf("%s:%d [getFrom] -> %s\n",inet_ntoa(peer_addr.sin_addr), ntohs(peer_addr.sin_port), p_socket->buffer);
}

/*____________________________________________________________________________
  Metoda vyčistí buffer socketu a nastaví index aktuálně čteného znaku
  na 0.
  ____________________________________________________________________________
 */
void server_clear_socket_mes_buf(playersocket_t *p_socket){
    memset(p_socket->buffer, '\0', MAX_MESSAGE_LEN);
    p_socket->cur_char_index = 0;
    p_socket->cur_mes_body_len = 0;
}

/*____________________________________________________________________________
  Hlavní smička serveru
  ____________________________________________________________________________
 */
return_code server_loop(int *server_socket){
    int i, max_sd, activity;
    int master_socket = *server_socket;
    int sd;
    time_t start_t, end_t;
    double diff;
    float fromStart;
    playersocket_t p_sockets[maximum_clients];
    fd_set readfds;
    struct timeval tv;




    for(i = 0; i < maximum_clients;i++){
        p_sockets[i].sd = 0;
    }

    while(1)
    {
        time(&start_t);
        tv.tv_sec = 1; /* čekání na aktivutu na selectu max 1s */
        tv.tv_usec = 0;
        FD_ZERO(&readfds);
        FD_SET(master_socket, &readfds);
        max_sd = master_socket;
        for ( i = 0 ; i < maximum_clients ; i++)
        {
            sd = p_sockets[i].sd;

            /* pokud je file deskriptor validní */
            if(sd > 0)
                FD_SET( sd , &readfds);

            /* Nejvyšší file deskriptor připraven na akci */
            if(sd > max_sd)
                max_sd = sd;
        }

        activity = select( max_sd + 1 , &readfds , NULL , NULL , &tv);

        if ((activity < 0))
        {
            printf("SERVER: run error! - select\n");
            return SERVER_RUN_ERR;
        }else if (activity > 0){ /* select zaznamenal nějakou aktivitu a tudíž je co číst a následně odesílat */
            if (connect_new_client(&master_socket, p_sockets, &readfds) != OK){
                return SERVER_RUN_ERR;
            }
            process_communication(p_sockets, &readfds);
        }

        time(&end_t) ;
        diff = difftime(end_t, start_t) * 1000;
        refresh_clients(p_sockets, diff);
        //lobby_resend_message(diff);
        lobby_check_rooms_state();
    }

    return 0;
}

/*____________________________________________________________________________
  Nastartuje server a spustí smyčku pro zpracování zpráv

  Vrací return_code:
  OK (0) - vše proběhlo v pořádku
  ILEGAL_PAR_VALUE (3) - nevalidní předaný parametr
  SERVER_START_ERR (4) - Nepodařilo se nastartovat server
  SERVER_RUN_ERR (5) - Došlo k chybě při běhu serveru
  ____________________________________________________________________________
 */
return_code server_init(int max_cli, char *ip, char *port){
    if(max_cli < 0 || max_cli > SERVER_LIMIT_CLIENTS){
        return ILEGAL_PAR_VALUE;
    }
    int server_socket;

    maximum_clients = max_cli;

    if(start_server(&server_socket, ip, port) != OK){
        return SERVER_START_ERR;
    }

    if(server_loop(&server_socket) != OK){
        return SERVER_RUN_ERR;
    }

    return OK;

}
