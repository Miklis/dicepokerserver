#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "protocol.h"
#include "player.h"
#include "server.h"
#include "lobby.h"
#include "utils.h"


/*____________________________________________________________________________
  Metoda kontroluje zda má tělo zprávy odpovídající délku
  ____________________________________________________________________________
 */
bool has_proper_lenght(playersocket_t *p_socket){
    int proper_len = p_socket->cur_mes_body_len + PROTOCOL_HEADER_LEN - 1;

    if(proper_len == p_socket->cur_char_index){
        return true;
    }else{
        return false;
    }
}

/*____________________________________________________________________________
  Převede stringovou podobu čísla reprezentující délku zprávy za hlavičkou do číselné podoby.
  ____________________________________________________________________________
 */
void body_len_to_int(playersocket_t *socket){
    char str_len[PROTOCOL_MES_BODY_LEN];
    int result;

    strncpy(str_len, (*socket).buffer + PROTOCOL_MAGIC_LEN + PROTOCOL_MES_TYPE_LEN, PROTOCOL_MES_BODY_LEN);

    result = atoi(str_len);
    (*socket).cur_mes_body_len = result;
}


/*____________________________________________________________________________
 Čte hlavičku zprávy.

 Vrací int:
 -1 Chybná hlavička zprávy
  0 Hlavička je v pořádku
  ____________________________________________________________________________
 */
int read_header(playersocket_t *p_socket, char *buffer){
    playersocket_t socket = *p_socket;
    int message_ch_index = socket.cur_char_index;

    //Kotrola magic hodnoty
    if(message_ch_index < PROTOCOL_MAGIC_LEN){
        if(PROTOCOL_MAGIC[message_ch_index] == buffer[0]){
            socket.buffer[message_ch_index] = buffer[0];
            socket.cur_char_index++;
        }else{
            /* zlej strejda nebo chybná zpráva utnout spojení */
            server_close_socket(p_socket, "header error");
            printf("%d", (*p_socket).sd);
            return -1;
        }
    /* Kontrola typu zprávy */
    }else if (message_ch_index < PROTOCOL_MAGIC_LEN + PROTOCOL_MES_TYPE_LEN){
        if(PROTOCOL_CONN[message_ch_index - PROTOCOL_MAGIC_LEN] == buffer[0]
        || PROTOCOL_LOBB[message_ch_index - PROTOCOL_MAGIC_LEN] == buffer[0]
        || PROTOCOL_DISC[message_ch_index - PROTOCOL_MAGIC_LEN] == buffer[0]
        || PROTOCOL_GAME[message_ch_index - PROTOCOL_MAGIC_LEN] == buffer[0]
        || PROTOCOL_PING[message_ch_index - PROTOCOL_MAGIC_LEN] == buffer[0]
        || PROTOCOL_RECO[message_ch_index - PROTOCOL_MAGIC_LEN] == buffer[0]){
            socket.buffer[message_ch_index] = buffer[0];
            socket.cur_char_index++;
        }else{
            /* zlej strejda nebo chybná zpráva utnout spojení */
            server_close_socket(p_socket, "header error");
            return -1;
        }
    /* Zpracování délky těla zprávy */
    }else if(message_ch_index < PROTOCOL_HEADER_LEN){
        if(buffer[0] >= '0' &&  buffer[0] <= '9'){
            socket.buffer[message_ch_index] = buffer[0];
            socket.cur_char_index++;

            if(message_ch_index == PROTOCOL_HEADER_LEN - 1){
                body_len_to_int(&socket);
            }
        }else{
            /* zlej strejda nebo chybná zpráva utnout spojení */
            server_close_socket(p_socket, "header error");
            return -1;
        }
    }

    *p_socket = socket;
    return 0;
}

/*____________________________________________________________________________
  Metoda sestavuje zprávu o informaci o stavu lobby podle protocolu
  a tuto zprávu posílá na klientský socket.
  ____________________________________________________________________________
 */
void protocol_send_lobby_info(playersocket_t *p_socket){
    int body_len;
    int mes_len = PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN + looby_room_num() * ONE_ROOM_INFO_SIZE + 2;//+1 je ukončení /n a \0
    int lobby_body_len = (looby_room_num() * ONE_ROOM_INFO_SIZE) + 1;
    char lobby_info_mes[mes_len];
    char body_str_len[PROTOCOL_MES_BODY_LEN + 1];
    char lobby_body[lobby_body_len];


    /* Nastavení magic */
    strncpy(lobby_info_mes, PROTOCOL_MAGIC, PROTOCOL_MAGIC_LEN);
    /* Nastavení typu zprávy */
    strncpy(lobby_info_mes + PROTOCOL_MAGIC_LEN, PROTOCOL_LOBB, PROTOCOL_MES_TYPE_LEN);

    /* Přídání do zprávy délku zprávy za hlavičkou */
    body_len = looby_room_num() * ONE_ROOM_INFO_SIZE + LOBBY_TYPE_LEN + 1; //počet místností * 4 + délka hlavičky lobby message + ukončení zprávy
    utils_convert_int_to_n_string(body_len, PROTOCOL_MES_BODY_LEN, body_str_len);
    strncpy(lobby_info_mes + PROTOCOL_MAGIC_LEN + PROTOCOL_MES_TYPE_LEN, body_str_len, PROTOCOL_MES_BODY_LEN);

    /** Tělo lobby části **/
    /* Nastavení hlavičky lobby části zprávy */
    strncpy(lobby_info_mes + PROTOCOL_HEADER_LEN, LOBBY_INFO, LOBBY_TYPE_LEN);
    lobby_get_info(lobby_body);
    /* Přidání těla lobby zprávy */
    strncpy(lobby_info_mes + PROTOCOL_HEADER_LEN + LOBBY_TYPE_LEN, lobby_body, lobby_body_len);
    lobby_info_mes[mes_len - 2] = '\n';
    lobby_info_mes[mes_len - 1] = '\0';

    //send((*p_socket).sd, lobby_info_mes, mes_len, 0);
    send_wl(p_socket, lobby_info_mes, mes_len - 1);
}

/*____________________________________________________________________________
  Metoda zpracovává zprávu pro připojení klienta.
  ____________________________________________________________________________
 */
void read_connecting_mess(playersocket_t *p_socket, char *buffer){
    /* Konec zprávy */
    if(buffer[0] == '\n'){
        recieved_fr(p_socket);
        if(has_proper_lenght(p_socket) == true){
            if(strncmp(p_socket->buffer, PROTOCOL_CH_CONN, 13) == 0){
                p_socket->state = IN_LOBBY;
                p_socket->buffer[p_socket->cur_char_index] = '\n';
                p_socket->buffer[p_socket->cur_char_index] = 0;
                p_socket->cur_char_index = 0;
                protocol_send_lobby_info(p_socket);
                server_clear_socket_mes_buf(p_socket);
            }else{
                server_close_socket(p_socket, "wrong header type");
                return;
            }
        }else{
            server_close_socket(p_socket, "too short message body");
            return;
        }

        /* čtení headeru zprávy  */
    }else if(p_socket->cur_char_index < PROTOCOL_HEADER_LEN){
        /* Chybná hlavička */
        if(read_header(p_socket, buffer) < 0){
            return;
        }
    }else{
        /* Čtení nicku*/
        if(p_socket->cur_char_index  <= p_socket->cur_mes_body_len + PROTOCOL_HEADER_LEN - 2){
                /* if(socket.cur_char_index - PROTOCOL_HEADER_LEN) */
                p_socket->player->nick[p_socket->cur_char_index - PROTOCOL_HEADER_LEN] = buffer[0];
                p_socket->buffer[p_socket->cur_char_index] = buffer[0];
                p_socket->cur_char_index++;
        }else{
            server_close_socket(p_socket, "too long message body");
        }
    }
}

/*____________________________________________________________________________
  Metoda připravuje hlavičku zprávy, což znamená že vyplňuje ve zprávě
  MAGIC, typ zprávy a délku těla.
  ____________________________________________________________________________
 */
void prepare_message_header(char *dest, char *mes_type ,int body_len) {
    char body_str_len[PROTOCOL_MES_BODY_LEN + 1];

    memset(body_str_len, '\0', PROTOCOL_MES_BODY_LEN + 1);



    strncpy(dest, PROTOCOL_MAGIC, PROTOCOL_MAGIC_LEN);
    strncpy(dest + PROTOCOL_MAGIC_LEN, mes_type, PROTOCOL_MES_TYPE_LEN);
    utils_convert_int_to_n_string(body_len, PROTOCOL_MES_BODY_LEN, body_str_len);

    strncpy(dest + PROTOCOL_MAGIC_LEN + PROTOCOL_MES_TYPE_LEN, body_str_len, PROTOCOL_MES_BODY_LEN);
}


/*____________________________________________________________________________
  Metoda posílá informaci o nicku soupeře na socket
  ____________________________________________________________________________
 */
void send_lobby_nick_mess(playersocket_t *p_socket, char *nick){
    int nick_len = strlen(nick);
    int body_len = LOBBY_TYPE_LEN + nick_len + 1; //+1 new line
    int mes_les = PROTOCOL_HEADER_LEN + body_len + 1; // +1 ukončení řetězce
    char lobby_nick_mes[mes_les];

    prepare_message_header(lobby_nick_mes, PROTOCOL_LOBB, body_len);

    strncpy(lobby_nick_mes + PROTOCOL_HEADER_LEN, LOBBY_NICK, PROTOCOL_MES_BODY_LEN);
    strncpy(lobby_nick_mes + PROTOCOL_HEADER_LEN + LOBBY_TYPE_LEN, nick, nick_len);

    lobby_nick_mes[mes_les - 2] = '\n';
    lobby_nick_mes[mes_les - 1] = '\0';
    //send((*p_socket).sd, lobby_nick_mes, mes_les, 0);
    send_wl(p_socket, lobby_nick_mes, mes_les - 1);
}

/*____________________________________________________________________________
  Metoda odešle zprávu typu LobbyRoomMessage, kdy na základě počtu
  hráčů v místnosti odešle hráči odpoveď jestli se může připojit do místnosti,
  připojit se a rovnou hrát nebo že v místnosti už není místo.
  ____________________________________________________________________________
 */
void send_lobby_room_mess(playersocket_t *p_socket, int room_id){
    int body_len = LOBBY_TYPE_LEN + LOBBY_ROOM_RES_LEN + 2 ; /* +2 odřádkování a ukončení řetězce  */
    int mes_len = PROTOCOL_HEADER_LEN + body_len;
    int room_res;
    char lobby_room_mes[mes_len];
    char body_str_len[PROTOCOL_MES_BODY_LEN + 1];
    playersocket_t *p_oponent = NULL;

    room_res = lobby_try_connect(p_socket, &p_oponent, room_id);

    prepare_message_header(lobby_room_mes, PROTOCOL_LOBB, body_len);

    strncpy(lobby_room_mes + PROTOCOL_HEADER_LEN, LOBBY_ROOM, LOBBY_TYPE_LEN);

    if(room_res == -1){
        strncpy(lobby_room_mes + PROTOCOL_HEADER_LEN + LOBBY_TYPE_LEN, LOBBY_ROOM_DEN, LOBBY_ROOM_RES_LEN);
    }else if(room_res == 1){
        strncpy(lobby_room_mes + PROTOCOL_HEADER_LEN + LOBBY_TYPE_LEN, LOBBY_ROOM_ACC, LOBBY_ROOM_RES_LEN);
        (*p_socket).state = WAITING_FOR_OP;
    }else if(room_res == 2){
        strncpy(lobby_room_mes + PROTOCOL_HEADER_LEN + LOBBY_TYPE_LEN, LOBBY_ROOM_STR, LOBBY_ROOM_RES_LEN);

        lobby_room_mes[mes_len - 2] = '\n';
        lobby_room_mes[mes_len - 1] = '\0';

        send_wl(p_socket, lobby_room_mes, mes_len - 1);
        send_wl(p_oponent, lobby_room_mes, mes_len - 1);
        send_lobby_nick_mess(p_socket, (*p_oponent).player->nick);
        send_lobby_nick_mess(p_oponent, (*p_socket).player->nick);
        return;
    }else if(room_res == 3){
        printf("multiple connection try \n"); /* Z nějaké důvodu se může stát že stejný hráč odešle více pokusů o připojení do lobby... nedělej nic */
        return;
    }else{
        printf("unexpected value\n");
    }

    lobby_room_mes[mes_len - 2] = '\n';
    lobby_room_mes[mes_len - 1] = '\0';

    //send((*p_socket).sd, lobby_room_mes, mes_len, 0);
    send_wl(p_socket, lobby_room_mes, mes_len - 1);
}

/*____________________________________________________________________________
  Metoda zpracovává zprávy typu lobby, kdy na základě typu přijaté zprávy
  provede odpovídající akci.
  ____________________________________________________________________________
 */
void procces_lobby_mess(playersocket_t *p_socket, bool waiting){
    int room_id;

    if(strncmp(p_socket->buffer, PROTOCOL_PING_ACC, PROTOCOL_PING_MES_LEN - 1) == 0){
        //recieved_fr(p_socket);
    }else if(strncmp(p_socket->buffer, PROTOCOL_PING_REQ, PROTOCOL_PING_MES_LEN - 1) == 0){
        //send((*p_socket).sd, PROTOCOL_PING_ACC_NL, PROTOCOL_PING_MES_LEN, 0);
        send_wl(p_socket, PROTOCOL_PING_ACC_NL, PROTOCOL_PING_MES_LEN);
    }/* zpráva typu request */
    else if(strncmp(p_socket->buffer + PROTOCOL_HEADER_LEN, LOBBY_REQS, LOBBY_TYPE_LEN) == 0 && waiting == false){
        /* zpráva požadující informace o lobby (informace of všech místnostech) */
        if(strncmp(p_socket->buffer + PROTOCOL_HEADER_LEN + LOBBY_TYPE_LEN, LOBBY_INFO, LOBBY_TYPE_LEN) == 0 && waiting == false){
            protocol_send_lobby_info(p_socket);
        /* zpráva která reprezentuje pokus o připojení do určité místnosti */
        }else if(strncmp(p_socket->buffer + PROTOCOL_HEADER_LEN + LOBBY_TYPE_LEN, LOBBY_ROOM, LOBBY_TYPE_LEN) == 0 && waiting == false){
            room_id  = utils_conver_str_room_num_to_int(p_socket);

            if(room_id < 1 || room_id > looby_room_num()){ /* v případě že by byl požadavek na místnost která neexistuje */
                server_close_socket(p_socket, "non existing room");
            }else{
                //room_res = lobby_try_connect(&socket, room_id);
                send_lobby_room_mess(p_socket, room_id);
            }
        }else{/* Zpráva neodpovídá žádnému typu očekávané zprávy */
            server_close_socket(p_socket, "unknown lobby reqs type");
        }

    }else if(strncmp(p_socket->buffer + PROTOCOL_HEADER_LEN, LOBBY_exit, LOBBY_TYPE_LEN) == 0) {
        lobby_remove_player_fr_room(p_socket);
        protocol_send_lobby_info(p_socket);

    }else if(strncmp(p_socket->buffer, PROTOCOL_DISC_MES, PROTOCOL_HEADER_LEN) == 0){
        server_close_socket(p_socket, "cliend send disconnection message");
    }else{/* Zpráva neodpovídá žádnému typu očekávané zprávy */
        server_close_socket(p_socket, "unknown lobby message type");
    }

    server_clear_socket_mes_buf(p_socket);
    //p_socket->cur_char_index = 0;
}


/*____________________________________________________________________________
  Metoda zpracovává zprávu typu LobbyRoomReqMessage
  ____________________________________________________________________________
 */
void read_lobby_mess(playersocket_t *p_socket, char *buffer, bool waiting){
    /* Konec zprávy */
    if(buffer[0] == '\n'){
        recieved_fr(p_socket);
        /* Kontrola že se jedná o zprávu typu Lobby */
        if(has_proper_lenght(p_socket) == true) {
            if (strncmp(p_socket->buffer, PROTOCOL_CH_LOBB, 13) == 0
             || strncmp(p_socket->buffer, PROTOCOL_CH_PING, 13) == 0
             || strncmp(p_socket->buffer, PROTOCOL_CH_DISC, 13) == 0) {
                procces_lobby_mess(p_socket, waiting);
                server_clear_socket_mes_buf(p_socket);
            } else {
                server_close_socket(p_socket, "unknown lobby message type");
            }
        }else{
            server_close_socket(p_socket, "too short message body");
        }
    }else if(p_socket->cur_char_index < PROTOCOL_HEADER_LEN) { /* Chybná hlavička */
        if (read_header(p_socket, buffer) < 0) {
            return;
        }
    }else{ /* čtení lobby message */
        if(p_socket->cur_char_index  <= p_socket->cur_mes_body_len + PROTOCOL_HEADER_LEN - 2){
            p_socket->buffer[p_socket->cur_char_index] = buffer[0];
            p_socket->cur_char_index++;
        }else{
            server_close_socket(p_socket, "too long message body");
        }
    }
}


/*____________________________________________________________________________
  Metoda pošle oponentovi zprávu obsahující informaci o hozené kombinaci
  ____________________________________________________________________________
 */
void send_game_dice_mes(playersocket_t *p_socket, playersocket_t *p_opponent){
    int mes_len = PROTOCOL_GAME_DICE_LEN + 1; //+1 konec řetězce
    char message[mes_len];

    memset(message, '\0', mes_len);

    prepare_message_header(message, PROTOCOL_GAME, PROTOCOL_GAME_DICE_BODY_LEN);

    strncpy(message + PROTOCOL_HEADER_LEN, GAME_TYPE_DICE, PROTOCOL_MES_BODY_LEN);
    strncpy(message + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN, &p_socket->player->dice1, 1);
    strncpy(message + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN + 1, &p_socket->player->dice2, 1);
    strncpy(message + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN + 2, &p_socket->player->dice3, 1);
    strncpy(message + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN + 3, &p_socket->player->dice4, 1);
    strncpy(message + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN + 4, &p_socket->player->dice5, 1);

    message[mes_len - 2] = '\n';
    send_wl(p_opponent, message, mes_len - 1);
}

/*____________________________________________________________________________
  Metoda pošle oponentovi zprávu obsahující informaci o vsazené částce
  ____________________________________________________________________________
 */
void send_game_cash_mes(playersocket_t *p_socket, playersocket_t *p_opponent){
    int mes_len = PROTOCOL_GAME_CASH_LEN + 1; //+1 konec řetězce
    char message[mes_len];

    memset(message, '\0', mes_len);

    prepare_message_header(message, PROTOCOL_GAME, PROTOCOL_GAME_CASH_BODY_LEN);
    strncpy(message + PROTOCOL_HEADER_LEN, GAME_TYPE_CASH, PROTOCOL_MES_BODY_LEN);
    strncpy(message + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN, p_socket->player->bet_money, 2);

    message[mes_len - 2] = '\n';

    send_wl(p_opponent, message ,mes_len - 1);
}

void send_game_stat_mes(playersocket_t *p_socket, playersocket_t *p_opponent){
    int mes_len = PROTOCOL_GAME_STAT_LEN + 1; // +1 konec řetězce
    char message[mes_len];

    memset(message, '\0', mes_len);
    prepare_message_header(message, PROTOCOL_GAME, PROTOCOL_GAME_STAT_BODY_LEN);
    strncpy(message + PROTOCOL_HEADER_LEN, GAME_TYPE_STAT, PROTOCOL_MES_BODY_LEN);
    strncpy(message + PROTOCOL_HEADER_LEN + PROTOCOL_MES_BODY_LEN, p_socket->buffer + PROTOCOL_HEADER_LEN  + PROTOCOL_MES_BODY_LEN, 4);

    message[mes_len - 2] = '\n';
    send_wl(p_opponent, message ,mes_len - 1);
}



/*____________________________________________________________________________
  Metoda zpracovává zprávy s hodnotami hozené kombinace na kostkách
  a následně předává zprávu oponentovi
  ____________________________________________________________________________
 */
void process_game_dice_mes(playersocket_t *p_socket, playersocket_t *opponent, room_t *room){
    if(p_socket == room->on_move){
        if(p_socket->recived_mes == false ){
            if(utils_get_dices_values(p_socket) == 0 ){
                p_socket->recived_mes = true;
                room->sending_to = opponent;
                room_copy_socket_buffer_to_room_buffer(p_socket, room);
                send_wl(p_socket, PROTOCOL_GAME_ACKN_NL, PROTOCOL_GAME_ACKN_LEN);
                send_game_dice_mes(p_socket, opponent);
            }else{
                server_close_socket(p_socket, "Invalid message or player trying cheat (dice - 1)");
            }
        }else{
            server_close_socket(p_socket, "Invalid message or player trying cheat (dice - 2)");
        }
    }else{
        server_close_socket(p_socket, "Invalid message or player trying cheat (dice - 3)");
    }
}


/*____________________________________________________________________________
  Metoda zpracovává zprávy s vsazenou částkou a předává informaci oponentovi
  zároveň se hodnota přečte a přičte se do celkové vsazené hodnoty room.
  ____________________________________________________________________________
 */
void process_game_cash_mes(playersocket_t *p_socket, playersocket_t *opponent, room_t *room){
    int return_val;
    int check_reise_val = 0;

    if(p_socket == room->on_move){
        if(p_socket->recived_mes == false) {
            return_val = utils_get_cash_value(p_socket, room);
            if(return_val == 0) {
                if (room->reise == true) {
                    check_reise_val = room->reised - atoi(p_socket->player->bet_money);
                    if (check_reise_val == 0) {
                        room->reise = false;
                    } else {
                        room->state = ROOM_EMPTY;
                        lobby_process_player_refuse_bet(p_socket, opponent); /* Protivník neakceptoval navýšení sázky */
                        return;
                    }
                }
                p_socket->recived_mes = true;
                room->sending_to = opponent;
                room_copy_socket_buffer_to_room_buffer(p_socket, room);
                send_wl(p_socket, PROTOCOL_GAME_ACKN_NL, PROTOCOL_GAME_ACKN_LEN);
                room_process_player_reise(p_socket, opponent, room);
                send_game_cash_mes(p_socket, opponent);
            }else if(return_val == 1) {
                lobby_process_player_refuse_bet(p_socket, opponent); /* Protivník neakceptoval navýšení sázky */
                return;
            }else{
                server_close_socket(p_socket, "Invalid message or player trying cheat (cash - 1)");
            }
        }else if(return_val == 1){
            room->state = ROOM_EMPTY;
            lobby_process_player_refuse_bet(p_socket, opponent);
        }else{
            server_close_socket(p_socket, "Invalid message or player trying cheat (cash - 2)");
        }
    }else{
        server_close_socket(p_socket, "Invalid message or player trying cheat (cash - 3)");
    }
}


/*____________________________________________________________________________
  Metoda zpracovává zprávy o tom kdo s hráčů vyhrál (zpracovává klient),
  server pak jen ověřuje na základě skóre jestli to sedí na základě předaného
  skóre.
  ____________________________________________________________________________
 */
void process_game_stat_mes(playersocket_t *p_socket, playersocket_t *opponent, room_t *room){
    if(p_socket->recived_mes == false){
        if(utils_get_stat_value(p_socket) == 0){
            p_socket->recived_mes = true;
            room->sending_to = opponent;
            room_copy_socket_buffer_to_room_buffer(p_socket, room);
            send_game_stat_mes(p_socket, opponent);
            send_wl(p_socket, PROTOCOL_GAME_ACKN_NL, PROTOCOL_GAME_ACKN_LEN);
        }else{
            server_close_socket(p_socket, "Invalid message or player trying cheat (stat)");
        }
    }
}


/*____________________________________________________________________________
  Metoda zpracovává zprávy potvrzení, po uskutečněné akci.
  Po tom co mají oba hráči příznak že ackn bylo doručeno se server přepne
  do dolšího stavu.
  ____________________________________________________________________________
 */
void process_game_ackn_mes(playersocket_t *p_socket, playersocket_t *opponent, room_t *room){
    p_socket->recive_ack = true;
    room->diff = 0;
    if(p_socket->recive_ack == true && opponent->recive_ack == true){
        player_process_players_ackn(room->player1, room->player2);
        room_resolve_next_state(room);
    }else{
        room->sending_to = NULL; /* Zpráva oponentovi došla už není třeba mu jí znovu posílat */
        room_switch_players(room);
    }
}

/*____________________________________________________________________________
  Metoda zpracovává zprávy s hlavičkou game
  ____________________________________________________________________________
 */
void process_game_mes(playersocket_t *p_socket){
    room_t *room;
    int room_id = -1;
    playersocket_t *opponent;
    lobby_get_player_room(p_socket, &room, &room_id);
    lobby_get_get_opponent(p_socket, &opponent);

    if(opponent == NULL){
     // návrat do lobby protože se protivník odpojil
        room_remove_lonely_player(p_socket);
    }

    if(strncmp(p_socket->buffer, PROTOCOL_PING_ACC, PROTOCOL_PING_MES_LEN - 1) == 0){

        //recieved_fr(p_socket);

    }else if(strncmp(p_socket->buffer, PROTOCOL_PING_REQ, PROTOCOL_PING_MES_LEN - 1) == 0){

        send_wl(p_socket, PROTOCOL_PING_ACC_NL, PROTOCOL_PING_MES_LEN);

    }else if(strncmp(p_socket->buffer + PROTOCOL_HEADER_LEN, GAME_TYPE_DICE, GAME_TYPE_LEN) == 0
         && (room->gameState == GAME_STATE_ROLL_ALL || room->gameState == GAME_STATE_ROLL)){

        /*********** GAME_DICE_MES  *****************/
        process_game_dice_mes(p_socket, opponent, room);

    }else if(strncmp(p_socket->buffer + PROTOCOL_HEADER_LEN, GAME_TYPE_CASH, GAME_TYPE_LEN) == 0
         && (room->gameState == GAME_STATE_BETS || room->gameState == GAME_STATE_REISE || room->gameState == GAME_STATE_START_ROUND) ){

        /*********** GAME_CASH_MES  *****************/
       process_game_cash_mes(p_socket, opponent, room);

    }else if(strncmp(p_socket->buffer + PROTOCOL_HEADER_LEN, GAME_TYPE_STAT, GAME_TYPE_LEN) == 0
         && (room->gameState == GAME_STATE_END_GAME || room->gameState == GAME_STATE_END_ROUND)){

        /*********** GAME_STAT_MES  *****************/
        process_game_stat_mes(p_socket, opponent, room);

    }else if(strncmp(p_socket->buffer + PROTOCOL_HEADER_LEN, GAME_TYPE_ACKN, GAME_TYPE_LEN) == 0){

        /*********** GAME_ACKN_MES  *****************/
        process_game_ackn_mes(p_socket, opponent, room);

    }else if(strncmp(p_socket->buffer + PROTOCOL_HEADER_LEN, LOBBY_exit, LOBBY_TYPE_LEN) == 0){
        lobby_remove_player_fr_room(p_socket);
        protocol_send_lobby_info(p_socket);
    }else{
        server_close_socket(p_socket, "Invalid message or player trying cheat (main)");
    }

    if(room->player1 != NULL && room->player2 != NULL){
        printf("Room[%d] - state(%d), money: %d, player1 wins: %d, player2 wins: %d\n",room_id ,room->gameState, room->money_total, room->player1->player->wins, room->player2->player->wins);
    }


}


/*____________________________________________________________________________
  Metoda čte zprávy typy game.
  ____________________________________________________________________________
 */
void read_game_mes(playersocket_t *p_socket, char *buffer){
    /* Konec zprávy */
    if(buffer[0] == '\n'){
        recieved_fr(p_socket);
        if(has_proper_lenght(p_socket) == true){
            /* Kontrola že se jedná o zprávu typu GAME, LOBB nebo PING */
            if(strncmp(p_socket->buffer, PROTOCOL_CH_GAME, 13) == 0
            || strncmp(p_socket->buffer, PROTOCOL_CH_LOBB, 13) == 0
            || strncmp(p_socket->buffer, PROTOCOL_CH_PING, 13) == 0){

                process_game_mes(p_socket);
                server_clear_socket_mes_buf(p_socket);
            }else{
                server_close_socket(p_socket, "unknown game message type");
            }
        }else{
            server_close_socket(p_socket, "too short message body");
        }
    }else if(p_socket->cur_char_index < PROTOCOL_HEADER_LEN) { /* Chybná hlavička */
        if (read_header(p_socket, buffer) < 0) {
            return;
        }
    }else{ /* čtení lobby message */
        if(p_socket->cur_char_index  <= p_socket->cur_mes_body_len + PROTOCOL_HEADER_LEN - 2){
            p_socket->buffer[p_socket->cur_char_index] = buffer[0];
            p_socket->cur_char_index++;
        }else{
            server_close_socket(p_socket, "too long message body");
        }
    }
}


/*____________________________________________________________________________
  Metoda posílá zprávu typu ping. Podle parametru request posílá výzvu
  na ping nebo odpověď.
  ____________________________________________________________________________
 */
void protocol_send_ping_mes(playersocket_t *p_socket, bool request){
    char mes[PROTOCOL_PING_MES_LEN];

    if(request == true){
        strncpy(mes, PROTOCOL_PING_REQ_NL, PROTOCOL_PING_MES_LEN);
    }else{
        strncpy(mes, PROTOCOL_PING_ACC_NL, PROTOCOL_PING_MES_LEN);
    }

    //send((*p_socket).sd, mes, PROTOCOL_PING_MES_LEN, 0);
    send_wl(p_socket, mes, PROTOCOL_PING_MES_LEN);
}


/*____________________________________________________________________________
  Čte přijaté znaky ze socketu po jednom a na základě stavu klientského
  socketu pak zpracovává zprávy dál.
  ____________________________________________________________________________
 */
void protocol_read_message(playersocket_t *p_socket){
    char buffer[1];
    if(read((*p_socket).sd , buffer, 1) == 0){
        server_close_socket(p_socket, "connection close");
        return;
    }else{
        p_socket->recived_bytes++;
        p_socket->milis_from_last_seen = 0.0F;
        p_socket->send_ping = 0;
    }

    switch ((*p_socket).state) {
        case CONNECTING:
            read_connecting_mess(p_socket, buffer);
            break;
        case IN_LOBBY:
            read_lobby_mess(p_socket, buffer, false);
            break;
        case PLAYING:
            read_game_mes(p_socket, buffer);
            break;
        case WAITING_FOR_OP: /* Hráč může například opustit místnost */
            read_lobby_mess(p_socket, buffer, true);
            break;
    }
}