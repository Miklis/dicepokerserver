//
// Created by miklis on 13.11.20.
//

#ifndef DICEPOKERSERVER_RETURN_CODE_H
#define DICEPOKERSERVER_RETURN_CODE_H

typedef enum {
    OK                          = 0,
    MISSING_ARGUMENT            = 1,
    FATAL_ERROR                 = 2,
    ILEGAL_PAR_VALUE            = 3,
    SERVER_START_ERR            = 4,
    SERVER_RUN_ERR              = 5
}return_code;


#endif //DICEPOKERSERVER_RETURN_CODE_H
