//
// Created by miklis on 13.11.20.
//

#ifndef DICEPOKERSERVER_PLAYER_H
#define DICEPOKERSERVER_PLAYER_H

#include <stdbool.h>


#define MAX_MESSAGE_LEN 128
#define PLAYER_NICK_MIN_LEN 4
#define PLAYER_NICK_MAX_LEN 24
#define PLAYER_TRY_PINGS 4
#define TIME_BETWEEN_PINGS 5000.0F

/* Stavy do kterých se hráč/hráčský socket může dostat */
typedef enum {
    CONNECTING                  = 0,
    IN_LOBBY                    = 1,
    WAITING_FOR_OP              = 2,
    PLAYING                     = 3,
    TRAING_RECO                 = 4,
}player_socket_state_t;

typedef  struct theplayer{
    char nick[24];
    char bet_money[3];
    int  wins;
    int  score;
    char dice1;
    char dice2;
    char dice3;
    char dice4;
    char dice5;
}player_t;

typedef struct theplayersocket{
    player_t *player;              /* Datová část hráče */
    int sd;                        /* filedeskriptor soketu (zkratka socket discriptor)*/
    int cur_char_index;            /* n-tý zpracovávaný znak */
    int cur_mes_body_len;          /* Délka zprávy za koncí headeru */
    int send_ping;                 /* Počet pingů kolik bylo odesláno přip pokusu o ověření připojení */
    float milis_from_last_seen;    /* Kolik uplynulo ms od poslední zprávy */
    char buffer[MAX_MESSAGE_LEN];  /* Buffer pro zpracovávanou zprávu */
    bool recive_ack;               /* Bylo přijato potvrzení od protistrany že byla doručena zpráva */
    bool recived_mes;              /* Byla přijata zpráva od socketu */
    unsigned long recived_bytes;   /* Počet přijatých B na socket hráče */
    unsigned long send_bytes;      /* Počet odeslaných B ze socketu hráče */
    player_socket_state_t state;   /* Stav ve kterým je socket hráče v rámci protoku*/

}playersocket_t;

void player_process_players_ackn(playersocket_t *player1, playersocket_t *player2);
#endif //DICEPOKERSERVER_PLAYER_H
