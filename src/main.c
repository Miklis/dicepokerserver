#include <stdio.h>
#include <stdlib.h>
#include "lobby.h"
#include "server.h"
#include "utils.h"

bool check_parametrs(char *ip, char *port, char *number_of_rooms){
    int port_num, room_num;

    if(utils_is_valid_ip_address(ip) == false){
        return false;
    }

    if(utils_validate_number(port) == true){
        port_num = atoi(port);

        if(port_num > 65535 || port_num < 1024){
            return false;
        }
    }else{
        return false;
    }

    if(utils_validate_number(number_of_rooms) == true){
        room_num = atoi(number_of_rooms);

        if(room_num < 1 || room_num > 25){
            return false;
        }
    }else{
        return false;
    }


    return true;
}


/*
 * první index argv == ip adresa, druhý port serveru, poslední počet místností ve hře
 *
 *
 */
int main(int argc, char **argv) {
    int room_num, players_num;
    if(argc == 4){
        if(check_parametrs(argv[1], argv[2], argv[3]) == true){
            room_num = atoi(argv[3]);
            players_num = room_num * 2;


            lobby_init(room_num);
            server_init(players_num, argv[1], argv[2]);
            lobby_clean();
        }else{
            printf("Some argument has incorect value.\n");
        }
    }else{
        printf("Missing arguments\n");
    }

    return 0;
}
