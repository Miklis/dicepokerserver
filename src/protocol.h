//
// Created by miklis on 14.11.20.
//

#ifndef DICEPOKERSERVER_PROTOCOL_H
#define DICEPOKERSERVER_PROTOCOL_H

#include <stdbool.h>
#include "player.h"


#define PROTOCOL_MAGIC_LEN     9
#define PROTOCOL_MES_TYPE_LEN  4
#define PROTOCOL_MES_BODY_LEN  4
#define PROTOCOL_HEADER_LEN    17
#define PROTOCOL_CH_LEN        13
#define PROTOCOL_PING_MES_LEN  21
#define PROTOCOL_GAME_DICE_LEN 27
#define PROTOCOL_GAME_CASH_LEN 24
#define PROTOCOL_GAME_STAT_LEN 26
#define PROTOCOL_GAME_OPPO_LEN 26
#define PROTOCOL_GAME_ACKN_LEN 22


#define PROTOCOL_GAME_DICE_BODY_LEN 10
#define PROTOCOL_GAME_CASH_BODY_LEN 7
#define PROTOCOL_GAME_STAT_BODY_LEN 9

#define GAME_TYPE_LEN 4

#define PROTOCOL_MAGIC "DICEPOKER"

/* Message types */
#define PROTOCOL_CONN "conn"
#define PROTOCOL_DISC "disc"
#define PROTOCOL_RECO "reco"
#define PROTOCOL_GAME "game"
#define PROTOCOL_LOBB "lobb"
#define PROTOCOL_PING "ping"

/* Message types with header  */
#define PROTOCOL_CH_CONN "DICEPOKERconn"
#define PROTOCOL_CH_DISC "DICEPOKERdisc"
#define PROTOCOL_CH_RECO "DICEPOKERreco"
#define PROTOCOL_CH_GAME "DICEPOKERgame"
#define PROTOCOL_CH_LOBB "DICEPOKERlobb"
#define PROTOCOL_CH_PING "DICEPOKERping"

/* Ping messages */

#define PROTOCOL_PING_REQ       "DICEPOKERping0004req"
#define PROTOCOL_PING_ACC       "DICEPOKERping0004acc"

/* Game ACKN messages */
#define PROTOCOL_GAME_ACKN_DICE "DICEPOKERgame0009ackndice"
#define PROTOCOL_GAME_ACKN_CASH "DICEPOKERgame0009ackncash"
#define PROTOCOL_GAME_ACKN_STAT "DICEPOKERgame0009acknstat"

/* Game opponent messages */
#define PROTOCOL_GAME_OPPO_DISC "DICEPOKERgame0009oppodisc"
#define PROTOCOL_GAME_OPPO_REFU "DICEPOKERgame0009opporefu"

/* Disconnection Message  */
#define PROTOCOL_DISC_MES       "DICEPOKERdisc0001"


/* Ping messages with new line */
#define PROTOCOL_PING_ACC_NL       "DICEPOKERping0004acc\n"
#define PROTOCOL_PING_REQ_NL       "DICEPOKERping0004req\n"




/* Game ACKN messages with new line*/
#define PROTOCOL_GAME_ACKN_NL      "DICEPOKERgame0005ackn\n"
#define PROTOCOL_GAME_ACKN_CASH_NL "DICEPOKERgame0009ackncash\n"
#define PROTOCOL_GAME_ACKN_STAT_NL "DICEPOKERgame0009acknstat\n"

/* Game opponent messages with new line */
#define PROTOCOL_GAME_OPPO_DISC_NL "DICEPOKERgame0009oppodisc\n"
#define PROTOCOL_GAME_OPPO_REFU_NL "DICEPOKERgame0009opporefu\n"

#define GAME_TYPE_DICE "dice"
#define GAME_TYPE_CASH "cash"
#define GAME_TYPE_STAT "stat"
#define GAME_TYPE_ACKN "ackn"

void protocol_send_ping_mes(playersocket_t *p_socket, bool request);
void protocol_read_message(playersocket_t *p_socket);
void protocol_send_lobby_info(playersocket_t *p_socket);

#endif //DICEPOKERSERVER_PROTOCOL_H
