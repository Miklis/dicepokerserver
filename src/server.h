//
// Created by miklis on 14.11.20.
//

#ifndef DICEPOKERSERVER_SERVER_H
#define DICEPOKERSERVER_SERVER_H
#include "return_code.h"

void server_close_socket(playersocket_t *p_socket, char *message);
void send_wl(playersocket_t *p_socket, char *mes, int mes_len);
void recieved_fr(playersocket_t *p_socket);
void server_clear_socket_mes_buf(playersocket_t *p_socket);
return_code server_init(int max_cli, char *ip, char *port);
#endif //DICEPOKERSERVER_SERVER_H
