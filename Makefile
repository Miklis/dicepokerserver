CC = gcc
BIN = DicePokerServer 
OBJ = $(wildcard src/*.c)
FLAGS =  -w -std=c99 

all: $(BIN) clean

$(BIN): $(OBJ)
	$(CC) $^ -o $@ $(FLAGS) -lm

%.o: %.c
	$(CC) -c $< -o $@ -lm

clean:
	rm -f *.o
	
